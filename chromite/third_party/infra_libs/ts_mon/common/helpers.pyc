ó
Ûë+]c           @   sR   d  Z  d d l m Z d d l Z d e f d     YZ d e f d     YZ d S(   sE   Helper classes that make it easier to instrument code for monitoring.iÿÿÿÿ(   t   metricsNt   ScopedIncrementCounterc           B   sD   e  Z d  Z d d d d  Z d   Z d   Z d   Z d   Z RS(	   s  Increment a counter when the wrapped code exits.

  The counter will be given a 'status' = 'success' or 'failure' label whose
  value will be set to depending on whether the wrapped code threw an exception.

  Example:

    mycounter = Counter('foo/stuff_done')
    with ScopedIncrementCounter(mycounter):
      DoStuff()

  To set a custom status label and status value:

    mycounter = Counter('foo/http_requests')
    with ScopedIncrementCounter(mycounter, 'response_code') as sc:
      status = MakeHttpRequest()
      sc.set_status(status)  # This custom status now won't be overwritten if
                             # the code later raises an exception.
  t   statust   successt   failurec         C   s1   | |  _  | |  _ | |  _ | |  _ d  |  _ d  S(   N(   t   countert   labelt   success_valuet   failure_valuet   NoneR   (   t   selfR   R   R   R   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyt   __init__"   s
    				c         C   s   |  j  |  j  d  S(   N(   t
   set_statusR   (   R
   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyt   set_failure*   s    c         C   s   | |  _  d  S(   N(   R   (   R
   R   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR   -   s    c         C   s   d  |  _ |  S(   N(   R	   R   (   R
   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyt	   __enter__0   s    	c         C   sZ   |  j  d  k r9 | d  k r* |  j |  _  q9 |  j |  _  n  |  j j i |  j  |  j 6 d  S(   N(   R   R	   R   R   R   t	   incrementR   (   R
   t   exc_typet	   exc_valuet	   traceback(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyt   __exit__4   s
    (   t   __name__t
   __module__t   __doc__R   R   R   R   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR      s   			t   ScopedMeasureTimec           B   s   e  Z d  Z i d e j j 6d e j j 6d e j j 6d e j j 6Z	 d d d d e
 j
 d  Z d	   Z d
   Z d   Z d   Z RS(   sI  Report durations metric with status when the wrapped code exits.

  The metric must be CumulativeDistributionMetric with a field to set status.
  The status field will be set to 'success' or 'failure' depending on whether
  the wrapped code threw an exception. The status field values can be customized
  with constructor kwargs or by calling `set_status`.

  A new instance of this class should be constructed each time it is used.

  Example:

    mymetric = CumulativeDistributionMetric(
      'xxx/durations', 'duration of xxx op'
      [StringField('status')],
      bucketer=ts_mon.GeometricBucketer(10**0.04),
      units=ts_mon.MetricsDataUnits.SECONDS)
    with ScopedMeasureTime(mymetric):
      DoStuff()

  To set a custom label and status value:

    mymetric = CumulativeDistributionMetric(
      'xxx/durations', 'duration of xxx op'
      [IntegerField('response_code')],
      bucketer=ts_mon.GeometricBucketer(10**0.04),
      units=ts_mon.MetricsDataUnits.MILLISECONDS)
    with ScopedMeasureTime(mymetric, field='response_code') as sd:
      sd.set_status(404)  # This custom status now won't be overwritten
                          # even if exception is raised later.

  To annotate the duration with some other fields, use extra_fields_values:

    mymetric = CumulativeDistributionMetric(
      'xxx/durations', 'duration of xxx op'
      [StringField('status'),
       StringField('type')],
      bucketer=ts_mon.GeometricBucketer(10**0.04),
      units=ts_mon.MetricsDataUnits.SECONDS)
    with ScopedMeasureTime(mymetric, extra_fields_values={'type': 'normal'}):
      DoStuff()
  g      ð?g     @@g    .Ag    eÍÍAR   R   R   c            sÿ   t  | t j  s t  t   f d   | j D  d k sM t d     | j |  j k s t d | j |  j j   f   | |  _	 t
 |  |  _   |  j k s® t  d  |  j   <  |  _ |  j | j |  _ | |  _ | |  _ d  |  _ | |  _ d  S(   Nc         3   s$   |  ] } | j    k r d  Vq d S(   i   N(   t   name(   t   .0t   spec(   t   field(    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pys	   <genexpr>s   s    i   s   typo in field name `%s`?s$   metric's units (%s) is not one of %s(   t
   isinstanceR    t   CumulativeDistributionMetrict   AssertionErrort   sumt
   field_spect   unitst   _UNITS_PER_SECONDt   keyst   _metrict   dictt   _field_valuesR	   t   _fieldt   _units_per_secondt   _success_valuet   _failure_valuet   _start_timestampt   _time_fn(   R
   t   metricR   R   R   t   extra_fields_valuest   time_fn(    (   R   sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR   o   s     (					c         C   s/   |  j  d  k	 s t d   | |  j |  j <d  S(   Ns4   set_status must be called only inside with statement(   R+   R	   R   R&   R'   (   R
   R   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR      s    	c         C   s   |  j  |  j  S(   N(   R   R*   (   R
   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR      s    c         C   s.   |  j  d  k s t d   |  j   |  _  |  S(   Ns.   re-use of ScopedMeasureTime instances detected(   R+   R	   R   R,   (   R
   (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR      s    c         C   s   |  j    |  j } |  j |  j d  k ra | d  k rK |  j |  j |  j <qa |  j |  j |  j <n  |  j j | |  j	 |  j  d  S(   N(
   R,   R+   R&   R'   R	   R)   R*   R$   t   addR(   (   R
   R   R   R   t   elapsed_seconds(    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR      s    (    (   R   R   R   R    t   MetricsDataUnitst   SECONDSt   MILLISECONDSt   MICROSECONDSt   NANOSECONDSR"   t   timeR   R   R   R   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyR   =   s   )			(   R   t   infra_libs.ts_mon.commonR    R7   t   objectR   R   (    (    (    sO   /home/dev01/chromiumos/chromite/third_party/infra_libs/ts_mon/common/helpers.pyt   <module>   s   0