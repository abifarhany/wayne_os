// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package chromiumos;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/chromiumos";

import "chromiumos/common.proto";
import "google/protobuf/wrappers.proto";

// Configuration used by a builder during execution.
message BuilderConfig {

  // Used to control running of special steps.
  enum RunSpec {
    RUN_SPEC_UNSPECIFIED = 0;
    // Do not run.
    NO_RUN = 1;
    // Run.
    RUN = 2;
    // Run and then exit.
    RUN_EXIT = 3;
  }

  // Unique identifier of the builder configuration.
  message Id {
    // The name of the configuration such as "arm-generic-postsubmit".
    string name = 1;
    // Branch the configuration applies to such as "release-R73-11647.B".
    string branch = 2;
    // Type of build.
    enum Type {
      TYPE_UNSPECIFIED = 0;
      // Commit queue build.
      CQ = 1;
      // Postsubmit build.
      POSTSUBMIT = 2;
    }
    Type type = 3;
  }
  Id id = 1;

  // General configuration for the builder.
  message General {
    // Whether or not the build is considered critical. A failure in a critical
    // build for a CQ builder may, for example, prevent submission of the CL.
    google.protobuf.BoolValue critical = 1;
  }
  General general = 2;

  // Configuration pertaining to builders operating as "orchestrators".
  // Orchestrators orchestrate the running of other child builders.
  message Orchestrator {
    // BuilderConfig.Id.name of the child builds. They would expect to match
    // this name and the BuilderConfig.Id.branch and BuilderConfig.Id.type of
    // this parent orchestrator builder.
    repeated string children = 1;
  }
  Orchestrator orchestrator = 3;

  // Configuration pertaining to artifacts produced during a build.
  message Artifacts {
    // How to handle the uploading of prebuilts.
    enum Prebuilts {
      PREBUILTS_UNSPECIFIED = 0;
      PUBLIC = 1;
      PRIVATE = 2;
      NONE = 3;
    }
    Prebuilts prebuilts = 1;

    // Used to indicate artifact types that should be uploaded by the builder.
    // Must be kept in sync with the dictionary in:
    // https://chromium.googlesource.com/chromiumos/infra/recipes/+/refs/heads/master/recipe_modules/cros_artifacts/api.py
    enum ArtifactTypes {
      ARTIFACT_TYPES_UNSPECIFIED = 0;
      // Indicates wanting a zip file of everything in the image directory.
      IMAGE_ZIP = 1;
      // Indicates wanting update payloads.
      TEST_UPDATE_PAYLOAD = 2;
      // Indicates wanting the autotest tarballs.
      AUTOTEST_FILES = 3;
      // Indicates wanting a tarball containing private TAST test bundles.
      TAST_FILES = 4;
      // Indicates wanting a tarball containing guest images and test bundles.
      PINNED_GUEST_IMAGES = 5;
      // Indicates wanting an archive of firmware images built from source.
      FIRMWARE = 6;
      // Indicates wanting a tarball of the Ebuilds logs.
      EBUILD_LOGS = 7;
    }
    repeated ArtifactTypes artifact_types = 2;

    // Google storage bucket to upload prebuilts to.
    string prebuilts_gs_bucket = 3;

    // Google storage bucket to upload artifacts to.
    string artifacts_gs_bucket = 4;
  }
  Artifacts artifacts = 4;

  // Configuration pertaining to Chrome.
  message Chrome {
    // Whether to build internal or external Chrome. Internal chrome adds
    // Chrome branding.
    bool internal = 1;
  }
  Chrome chrome = 5;

  // Build specific configuration.
  message Build {
    // USE flags to use with the build.
    repeated UseFlag use_flags = 1;

    // The profile of the variant to set up and build.
    message PortageProfile {
      string profile = 1;
    }
    PortageProfile portage_profile = 2;

    // Image types to build.
    repeated ImageType image_types = 3;

    // Controls the running of the install packages step.
    RunSpec install_packages = 4;

    // Compile toolchain from source (true), or use bin packages (false).
    bool compile_toolchain = 5;

    // Whether to apply gerrit changes, if any. Use case is to build without
    // changes after failure to identify the changes as the culprit.
    bool apply_gerrit_changes = 6;
  }
  Build build = 6;

  message UnitTests {
    // Skipped packages.
    repeated PackageInfo package_blacklist = 4;

    // Controls the running of ebuild tests with possible early exit.
    RunSpec ebuilds_run_spec = 5;

    // Whether to assume the sysroot is empty.
    bool empty_sysroot = 6;
  }
  UnitTests unit_tests = 7;
}

// List of builder configs. Intended to be serialized to / from disk.
message BuilderConfigs {
  repeated BuilderConfig builder_configs = 1;
}
