This directory contains the sources for the new-style BIOS bitmaps, and a
simple (and ugly) tool to view the configuration file that describes how
each screen is displayed.

Note:

Due to continuing improvements and tweaks, there have been several different
formats used for the BIOS bitmaps.

Because the bitmap images and display code is part of the Read-Only BIOS,
back-porting any new bitmaps to older devices is not possible.


Old-style, un-versioned bitmaps. Used in Mario / Cr-48.

In the Cr-48 BIOS there are four BIOS screens that may be presented to the
user. Each contains a graphic, a URL, and some informative text. The screens
are single bitmap images, hard-coded in read-only BIOS (because they have to
display even when the R/W BIOS and SSD are both completely erased). They can
be replaced at manufacturing time, but creating the screens is difficult.
The format is a compressed EFI firmware volume that is generated when the
BIOS is compiled. The result is an opaque blob that cannot be viewed or
edited with linux-based tools.


Version 1.0, new-style bitmaps. Used in Alex / Samsung Series 5.

The BIOS continues to display the same basic screens, but it uses a
different format internally (which we call the bmpblock). Each screen has
separate bitmaps for the basic graphic, the URL, and the informative text,
and the screen is displayed by rendering each component in order. This
allows us to modify and replace any bitmap (most frequently the HWID), using
standard command-line linux tools such as imagemagick. Compositing each
screen in this way also means that we can easily provide localized BIOS
screens or custom messages. The BIOS rotates through the localizations by
pressing the arrow keys when any screen is displayed.

* To build new Alex bitmaps, check out "chromeos/autotest-private-x86-alex"
  on branch "0.11.241.B", and see the tools in
  client/site_tests/hardware_Components/utils.
  Reference: https://chrome-internal-review.googlesource.com/23243/


Version 1.1. Used in ZGB / Acer AC700.

This is essentially the same as version 1.0, except that the ASCII HWID
string can be rendered directly by the BIOS instead of as a bitmap. In the
screen description, the magic image name "$HWID" (or "$HWID.rtol" for a
right-justified placement) indicates that the ASCII HWID value should be
displayed at the given coordinates instead of a bitmap image. This means
that we only need to generate one bmpblock for all locales, since the ASCII
HWID string can be changed at the factory using "futility gbb". The
last-displayed locale is stored in nvram, so it's sticky across reboots. The
factory process sets the default locale to the appropriate region.


Version 1.2. Used by any BIOS that uses "vboot_api.h" (ARM, Stumpy, etc.)

The "vboot wrapper" is a refactoring of the vboot_reference library to
isolate our verified boot stuff from the underlying BIOS. Among other
things, it places the burden of displaying the ASCII HWID value on the
vboot_reference library, which means the bmpblock must contain a "font" to
translate the ASCII characters in the HWID into graphical images. The yaml
file must now specify a font file for the $HWID (and $HWID.rtol, if used)
image name. For example:

  bmpblock: 1.2
  images:
    $HWID: hwid_fonts.font          # THIS IS NOW REQUIRED WHEN USING $HWID
  screens:
    scr_0_0:
     - [ 0, 0, $HWID]
  localizations:
    - [ scr_0_0, scr_0_0, scr_0_0, scr_0_0 ]
  locale_index:
    en

The old v1.1 bmpblock will be accepted by the vboot wrapper, but a $HWID
screen without a corresponding font should be silently ignored.


Version 2.0. Used by BIOS with complex composition (Kiev, Link, Snow, etc.)

The max number of components in one screen has increased from 8 to 16, makes
bitmap block structure incompatible with previous versions.



--------------------------------------------
Instructions for manually rebuilding things:

It is now easier to build bmpblk INSIDE chroot. Just run "make" and everything
will be regenerated:

  (chroot) cd ~/trunk/src/platform/bmpblk
  (chroot) make

This should generate BIOS bmpblock file for all platforms cross all locales,
which takes a long time.

If you simply want to build image for single target with default locales, do:

  (chroot) cd ~/trunk/src/platform/bmpblk
  (chroot) make $BOARD

Where $BOARD is the board name of your target, ex: "link". There is one simple
reference target "std" with fewer locales so you can test the building procedure
faster: "make std".

The default generated output files are in ./build/$BOARD. To override output
folder, just specify OUTPUT=/path_to_output in Make variables.

Note if you want to override the default locales list defined in boards.yaml
(for instance, to build with only English locale to speed up testing flow, or
to try with all available locales), add LOCALES="<locale-list>" when running
make.
Example:

  make LOCALES="en ja es" make link

Note if you want to use the "bitmap viewer" utility, since we don't have
wxWidgets and X environment inside SDK, it's currently only available OUTSIDE
chroot and you have to install these packages manually.  For example, on
Ubuntu systems, do:

  sudo apt-get install python-yaml python-wxgtk2.8

And then you can preview the bitmaps by following commands (outside chroot):

  ./bitmapviewer build/$BOARD/DEFAULT.yaml 1366x768

-------------------
Adding a new target:

The following properties of the new target need to be known in
advance:


 - dimensions of display panel in pixels.

  These are dimensions the firmware initializes the panel to. On ARM
  platforms dimensions usually match the actual panel hardware.

  On X86 things are a little more complicated. To determine actual
  boot screen dimensions first find the setting for
  CONFIG_FRAMEBUFFER_VESA_MODE in coreboot/configs/config.YOUR_BOARD

  Then look up the dimension for the found VESA in
  http://en.wikipedia.org/wiki/VESA_BIOS_Extensions#Modes_defined_by_VESA

  Note if the panel is much larger (for example, 1920x1080) than default
  resolution (1366x768), you may want to use a higher resolution of image
  resources (icons, diagrams, ... etc). To do that, change the "assets_dir"
  property.

  Currently we have two directories in images/ containing the image resources
  in different resolution: "assets" (designed for 1366x768) and "assets2x"
  (double sized from "assets").

  When using "assets2x", you should also override "assets_res" to specify the
  dimension that best fits given assets (hint: setting "assets_res" to same
  value as graphics resolution "res" would not resize anything).


 - device boot ability

  by default the device should be able to boot into recovery mode from
  USB and/or SD interfaces. But some devices do not have certain
  interfaces, some devices can't boot from certain interfaces. That is
  standard directions for the user shown on the boot screen need to be
  amended.


 - set of locales the device boot screen is required to support

  Country locales can be found in
  https://chromium.googlesource.com/chromium/src.git/+/master/ui/base/l10n/l10n_util.cc
  or http://www.science.co.il/language/locale-codes.asp


To add a new target (board), edit images/boards.yaml and fill the information:

------------------------------------------------------------------------------
link:
  # Non-standard Graphics mode 1280x850, with 2560x1700 panel.
  # Can boot recovery by USB and SD card readers.
  res:      [1280, 850]
  panel:    [2560, 1700]
  locales: [en, es-419, pt-BR, fr, es, it, de, nl, da, 'no', sv, ko, ja, id, th]
------------------------------------------------------------------------------

The locale name <no> will be interpreted as boolean False in YAML, so we need to
quote it as 'no'.

If your configuration is exactly the same as existing ones, add your new target
name into the existing entry, ex (4 targets with same configuration: falco,
peppy, wolf, leon):

------------------------------------------------------------------------------
falco,peppy,wolf,leon:
  # Using VESA graphics mode 1024x768 (0x0117), streched to 1366x768 panel.
  # With card reader. USB3 ports will run in USB2 mode for recovery boot.
  res:      [1024, 768]
  panel:    [1366, 768]
------------------------------------------------------------------------------

Now to build images for the new target (as $BOARD) run:

  $ TARGETS="$BOARD" make

 or specify the target directly,

  $ make "$BOARD"

The bitmaps and output binary (bmpblk.bin) can be found in ./build/$BOARD.

You can preview the images by running Bitmap Viewer OUTSIDE chroot:
  $ ./bitmap_viewer build/$BOARD/DEFAULT.yaml

Information about integrating new bitmaps into the firmware images can
be found in http://goo.gl/yXBvz


------------------------------------
Handling generated bitmaps:

You can use futility to modify your BIOS to contain this new set of bitmaps:

  futility gbb -s -b PATH/TO/YOUR/bmpblk.bin OLDBIOS.bin NEWBIOS.bin



You can do that last step on the chromebook device itself, provided that the
BIOS write-protection is disabled. futility should be in the rootfs:

  Become root:

    sudo bash

  Copy your new bmpblock over:

    cd /mnt/stateful_partition
    scp USER@SOMEHOST:/SOMEPATH/bmpblk.bin .

  Get a copy of the current BIOS:

    flashrom -p host -r bios.bin

  Put the new bmpblock in the copy of the BIOS:

    futility gbb -s -b bmpblk.bin bios.bin

  Reflash the BIOS with the new content

    flashrom -p host -w bios.bin

  Reboot. You should see your new bitmaps appear whenever the BIOS screens
  are displayed. If you have more than one localization, you should be able
  to cycle among them with the arrow keys.


If you want to examine a binary bmpblock that you've pulled from a BIOS
image, the bmpblk_utility has options to display or unpack the binary.

  bmpblk_utility bmpblk.bin

  bmpblk_utility -y bmpblk.bin

  bmpblk_utility -x -d /SOME/SCRATCH/DIR bmpblk.bin

Once you've unpacked it you can use the bitmap_viewer on the unpacked
config.yaml file to see what it looks like. There's not (yet) a single tool
that directly displays the raw binary.

------------------------------------------------------------------------------
If you want to build OUTSIDE chroot, here's some information:

On Ubuntu, you need to install following packages:

  sudo apt-get install libpango1.0-dev python-imaging librsvg2-bin

And you probably want to make sure all required fonts are installed:

  sudo apt-get install fonts-noto

Currently Ubuntu Trusty (14.04) does not have some of required font files
(Arabic UI and CJK) so you have to manually install them:
  mirror=http://commondatastorage.googleapis.com/chromeos-localmirror/distfiles
  wget "${mirror}/notofonts-20140815.tar.bz2"
  wget "${mirror}/noto-cjk-20140912.tar.bz2"
  sudo tar -xvf notofonts-20140815.tar.bz2 -C /usr/share/fonts/truetype
  sudo tar -xvf noto-cjk-20140912.tar.bz2 -C /usr/share/fonts/truetype
  sudo fc-cache  # Reload font cache.

If you want to use the "bitmap viewer" utility, add these packages:

  sudo apt-get install python-yaml python-wxgtk2.8

We also need some utilities from ChromiumOS source tree. You need to install
dependency libraries for them:

  sudo apt-get install libtspi-dev uuid-dev libyaml-dev

On older Ubuntu installations you would also need liblzml-dev, on newer ones -
liblzma-dev, try installing both, one at a time, at least one must succeed.

Now, time to build the utilities in vboot_reference outside of the chroot:

  cd src/platform/vboot_reference
  make

Once you've built vboot_reference you'll find the executables in
./build/utility/bmpblk*. If they were built in the standard place they
will be found by this tool automatically. If not, their location must be added
to your $PATH.
