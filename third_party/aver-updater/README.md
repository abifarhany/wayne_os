aver-updater is a utility to upgrade aver camera firmwares.

## Requirements
The GNU C/C++ library is required.

## Building
At the top level of the directory.
```
$ make
```
Alternatively at Chromium OS development environment,
```
$ emerge-${BOARD} aver-updater
