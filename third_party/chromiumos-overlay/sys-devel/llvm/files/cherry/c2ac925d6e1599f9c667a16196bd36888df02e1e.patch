From c2ac925d6e1599f9c667a16196bd36888df02e1e Mon Sep 17 00:00:00 2001
From: Vitaly Buka <vitalybuka@google.com>
Date: Fri, 12 Jul 2019 17:21:55 +0000
Subject: [PATCH] CodeGet: Init 32bit pointers with 0xFFFFFFFF

Summary:
Patch makes D63967 effective for 32bit platforms and improves pattern
initialization there. It cuts size of 32bit binary compiled with
-ftrivial-auto-var-init=pattern by 2% (3% with -Os).

Binary size change on CTMark, (with -fuse-ld=lld -Wl,--icf=all, similar results with default linker options)
```
                   master           patch      diff
Os pattern   7.915580e+05    7.698424e+05 -0.028387
O3 pattern   9.953688e+05    9.752952e+05 -0.019325
```

Zero vs Pattern on master
```
               zero       pattern      diff
Os     7.689712e+05  7.915580e+05  0.031380
O3     9.744796e+05  9.953688e+05  0.021133
```

Zero vs Pattern with the patch
```
               zero       pattern      diff
Os     7.689712e+05  7.698424e+05  0.000789
O3     9.744796e+05  9.752952e+05  0.000742
```

Reviewers: pcc, eugenis, glider, jfb

Reviewed By: jfb

Subscribers: hubert.reinterpretcast, dexonsmith, cfe-commits

Tags: #clang

Differential Revision: https://reviews.llvm.org/D64597

llvm-svn: 365921
---
 clang/lib/CodeGen/PatternInit.cpp       | 28 +++++++++----------------
 clang/test/CodeGenCXX/auto-var-init.cpp |  2 +-
 2 files changed, 11 insertions(+), 19 deletions(-)

diff --git a/clang/lib/CodeGen/PatternInit.cpp b/clang/lib/CodeGen/PatternInit.cpp
index 7a1baf96cfd..3410c7f2153 100644
--- a/clang/lib/CodeGen/PatternInit.cpp
+++ b/clang/lib/CodeGen/PatternInit.cpp
@@ -17,12 +17,13 @@ llvm::Constant *clang::CodeGen::initializationPatternFor(CodeGenModule &CGM,
   // repeated byte-pattern which makes it easier to synthesize. We use it for
   // pointers as well as integers so that aggregates are likely to be
   // initialized with this repeated value.
-  constexpr uint64_t LargeValue = 0xAAAAAAAAAAAAAAAAull;
   // For 32-bit platforms it's a bit trickier because, across systems, only the
-  // zero page can reasonably be expected to be unmapped, and even then we need
-  // a very low address. We use a smaller value, and that value sadly doesn't
-  // have a repeated byte-pattern. We don't use it for integers.
-  constexpr uint32_t SmallValue = 0x000000AA;
+  // zero page can reasonably be expected to be unmapped. We use max 0xFFFFFFFF
+  // assuming that memory access will overlap into zero page.
+  const uint64_t IntValue =
+      CGM.getContext().getTargetInfo().getMaxPointerWidth() < 64
+          ? 0xFFFFFFFFFFFFFFFFull
+          : 0xAAAAAAAAAAAAAAAAull;
   // Floating-point values are initialized as NaNs because they propagate. Using
   // a repeated byte pattern means that it will be easier to initialize
   // all-floating-point aggregates and arrays with memset. Further, aggregates
@@ -36,27 +37,18 @@ llvm::Constant *clang::CodeGen::initializationPatternFor(CodeGenModule &CGM,
                             Ty->isVectorTy() ? Ty->getVectorElementType() : Ty)
                             ->getBitWidth();
     if (BitWidth <= 64)
-      return llvm::ConstantInt::get(Ty, LargeValue);
+      return llvm::ConstantInt::get(Ty, IntValue);
     return llvm::ConstantInt::get(
-        Ty, llvm::APInt::getSplat(BitWidth, llvm::APInt(64, LargeValue)));
+        Ty, llvm::APInt::getSplat(BitWidth, llvm::APInt(64, IntValue)));
   }
   if (Ty->isPtrOrPtrVectorTy()) {
     auto *PtrTy = cast<llvm::PointerType>(
         Ty->isVectorTy() ? Ty->getVectorElementType() : Ty);
     unsigned PtrWidth = CGM.getContext().getTargetInfo().getPointerWidth(
         PtrTy->getAddressSpace());
-    llvm::Type *IntTy = llvm::IntegerType::get(CGM.getLLVMContext(), PtrWidth);
-    uint64_t IntValue;
-    switch (PtrWidth) {
-    default:
+    if (PtrWidth > 64)
       llvm_unreachable("pattern initialization of unsupported pointer width");
-    case 64:
-      IntValue = LargeValue;
-      break;
-    case 32:
-      IntValue = SmallValue;
-      break;
-    }
+    llvm::Type *IntTy = llvm::IntegerType::get(CGM.getLLVMContext(), PtrWidth);
     auto *Int = llvm::ConstantInt::get(IntTy, IntValue);
     return llvm::ConstantExpr::getIntToPtr(Int, PtrTy);
   }

