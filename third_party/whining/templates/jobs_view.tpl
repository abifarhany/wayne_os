%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='jobs')

  %#-------------------------------------------------------------------------
  %# A table with job names and links to jobs.
  %#-------------------------------------------------------------------------
  %_table_data = tpl_vars['data']['table_data']
  %_top_title = 'Job Names - %s jobs' % len(_table_data.row_headers)
  %_cell_classes = {'_default': 'lefted'}
  %_head_classes = {'_default': 'headeritem lefted'}
  %_ignore_cols = ['row_key']
  {{! _table_data.simple_table(_top_title, _cell_classes, _head_classes, [], _ignore_cols) }}
%end

%rebase('master.tpl', title='jobs', query_string=query_string, body_block=body_block)
