%# Copyright 2015 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%
%from src import settings
%_root = settings.settings.relative_root

<div id="maia-signature"></div>
<div class="maia-footer" id="maia-footer">
  <div id="maia-footer-global">
    <div class="maia-aux">
      <ul>
        <li><a href="{{ _root }}/">Home</a></li>
        <li><a href="{{ _root }}/help">Help</a></li>
        <li>Chromium OS test result viewer</li>
      </ul>
    </div>
  </div>
</div>
